import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ManagementsService {
    // pathPrefix: any = `:40014`
    pathPrefixAuth: any = `:40001`;
    currentDate: string = new Date().toISOString().substring(0,10);

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

    async list(hospitalID:any,serviceID:any) {
        this.currentDate = "2024-01-10";
        let data:any = {
            "reserve_date":this.currentDate,
            "hospital_id":hospitalID,
            "service_id":serviceID
        }
        
        const url = `/backend/getManagementReserve`;
        return this.axiosInstance.post(url,data);
    }

    async getById(id: any) {
        const url = `/` + id;
        return this.axiosInstance.get(url);
    }

    async save(data: any) {
        const url = `/`;
        return this.axiosInstance.post(url, data);
    }

    async update(id: any, data: any) {
        const url = `/reserves/` + id;
        return this.axiosInstance.put(url, data);
    }
}
