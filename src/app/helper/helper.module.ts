import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelperRoutingModule } from './helper-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        HelperRoutingModule
    ]
})
export class HelperModule {
}
