import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineModule } from 'primeng/timeline';

import { AppsRoutingModule } from './apps-routing.module';


@NgModule({
  imports: [
    CommonModule,
    AppsRoutingModule,
    TimelineModule
  ],
  declarations: [

  ]
})
export class AppsModule { }
