import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListBookingComponent} from './list-booking.component'

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild([
    { path: '', component: ListBookingComponent }
])],
  exports: [RouterModule]
})
export class ListBookingRoutingModule { }
