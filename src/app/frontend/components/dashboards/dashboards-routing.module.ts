import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', data: {breadcrumb: 'Main Dashboard'}, loadChildren: () => import('./main/main.module').then(m => m.MainModule) },
        // { path: 'dashboard-sales', data: {breadcrumb: 'Sales Dashboard'}, loadChildren: () => import('./sales/sales.dashboard.module').then(m => m.SalesDashboardModule) }
    ])],
    exports: [RouterModule]
})
export class DashboardsRoutingModule { }